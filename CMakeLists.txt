cmake_minimum_required(VERSION 2.6)

project(LabPF)

find_package(OpenCV REQUIRED)
message(STATUS "OpenCV include: " ${OpenCV_INCLUDE_DIRS})

find_package(OpenMP)
if (OPENMP_FOUND)
    include_directories(${OPENGM_INCLUDE_DIR})
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    add_definitions(-DUSEOMP)
endif()

include_directories(${OpenCV_INCLUDE_DIRS})
link_directories(${OpenCV_LIBRARY_DIRS})

file(GLOB srcs "*.cpp" "*.h")

add_executable(main ${srcs})
target_link_libraries(main ${OpenCV_LIBRARIES})
