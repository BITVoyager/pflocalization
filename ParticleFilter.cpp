/**
 * @file ParticleFilter.cpp
 * @date Feb. 2016
 * @author Zhaoyang Lv, Yao Chen
 */

#include "ParticleFilter.h"
#include "vis.h"
#include "timer.h"

#include <opencv2/core/types.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <limits>

#ifdef USEOMP
#include <omp.h>
#endif

using namespace std;

#define INF_INT  std::numeric_limits<int>::max();

double Particle::ts = 0;
double Particle::x_off = 0;
double Particle::y_off = 0;
static TimerStats Stats;

/* ************************************************************************* */
void ParticleFilter::initParticles(int size) {

  cout << "create " << size << " numbers of particles" << endl;
  cout << endl;
  srand(time(NULL));

  int num = 0;
  while(num < size) {
    Particle p;
    p.x = rand() % map.size_x;
    p.y = rand() % map.size_y;

    if (!map.isFreeSpace(p.x, p.y)) continue;

    p.theta = (rand() % (180)) / 90.0 * M_PI - M_PI;  //[-pi, pi]
    p.w = 1.0;      // initial weight all 1 / N

    particles.push_back(p);
    num ++;
  }

  // normalize weights
  double sumWeights = 0.0;
  size_t nrParticles = particles.size();
  for (size_t pIdx = 0; pIdx < nrParticles; pIdx++) {
    sumWeights += particles[pIdx].w;
  }
  for (size_t pIdx = 0; pIdx < nrParticles; pIdx++) {
    particles[pIdx].w = particles[pIdx].w / sumWeights;
  }

  cout << "The final number of particles is " << particles.size() << endl;
}

/* ************************************************************************* */
void ParticleFilter::run() {
  const double start = Stats.start();
  assert(readings.size() > 1);

  for (int i = 1; i < readings.size(); i++) {

    cout << "Processing reading No. " << i << endl;
    const double startProcessing = Stats.sample("1. particle filter");

    const Reading& r = readings[i];

    // update the time stamp
    Particle::ts = r.ts;

    // update motion
    bool robotMoved = updateMotionModel(r, readings[i - 1]);
    Stats.sample("2. update motion");

    // update weights based on measurement model
    if (r.isLaser()) {
      updateMeasurementModel(r);
      Stats.sample("3. update weights");

      if (robotMoved) {
        resampling(); // only resample when measurement comes in
      }
      Stats.sample("4. resampling");
    }

    displayParticles();
    const double duration = Stats.sample("Visualize") - startProcessing;

    Stats.sample("total", duration, TimerStats::TIME);

    Stats.print();
    Stats.reset();
  }

}

/* ************************************************************************* */
void ParticleFilter::displayParticles(int wait) {
  cv::Mat disp = visMap.clone();

  for (int i = 0; i < particles.size(); i++) {
    const Particle& p = particles[i];
    cv::circle(disp, cv::Point2d(p.x, map.size_y - p.y - 1), 1, cv::Scalar(0, 0, 255));
  }

  cv::imshow("particles  map", disp);
  cv::waitKey(wait);

  //visualizeReadings(readings, map);
}

/* ************************************************************************* */
void ParticleFilter::displayRays(int x, int y, int theta, const vector<int>& zs,
                                 int wait) {
  cv::Mat disp = visMap.clone();
  cv::Point2i pO(x, map.size_y-y);
  cv::circle(disp, pO, 5, cv::Scalar(0, 0, 255), cv::FILLED);
  for (int k = 0; k < 180; k++) {
    int v = zs[k] / 10;
    double angle = theta + (k-90.0) / 180.0 * M_PI;
    cv::Point2i end(x + v*cos(angle), map.size_y - (y + v*sin(angle)));
    cv::line(disp, pO, end, cv::Scalar(0, 255, 0));
  }
  cv::imshow("display rays for a particle", disp);
  cv::waitKey(wait);
}

/* ************************************************************************* */
bool ParticleFilter::updateMotionModel(const Reading& currOdometry,
                                       const Reading& lastOdometry) {
  bool robotMoved = true;

  // calculate shifts in odometry frame
  double dy = (currOdometry.y_r - lastOdometry.y_r) / 10.0; // the map is downsampled by 10
  double dx = (currOdometry.x_r - lastOdometry.x_r) / 10.0; // the map is downsampled by 10

  // atan2() gives the resulting angles in [-pi, +pi]
  double deltaRot1 = atan2(dy, dx) - lastOdometry.theta_r;
  double deltaTrans = sqrt(dy * dy + dx * dx);
  double deltaRot2 = currOdometry.theta_r - lastOdometry.theta_r - deltaRot1;

  robotMoved = fabs(deltaRot1) > 1e-2 && fabs(deltaTrans) > 1e-2 &&
      fabs(deltaRot2) > 1e-2;

  // define 4 odometry-specified parameters and draw samples
  double a1 = 1;
  double a2 = 1;
  double a3 = 1;
  double a4 = 1;

  static cv::RNG rng;
  // update the state of a particle
#ifdef USEOMP
#pragma omp parallel for num_threads(params.nThreads)
#endif
  for (size_t pIdx = 0; pIdx < particles.size(); pIdx++) {
    Particle& pc = particles[pIdx];
    double variance1 = a1 * deltaRot1 * deltaRot1 + a2 * deltaTrans * deltaTrans;
    double ddeltaRot1_ = deltaRot1 - rng.gaussian(sqrt(variance1));

    double variance2 = a3 * deltaTrans * deltaTrans + a4 * deltaRot1 * deltaRot1 +
        a4 * deltaRot2 * deltaRot2;
    double ddeltaTrans_ = deltaTrans - rng.gaussian(sqrt(variance2));

    double variance3 = a1 * deltaRot2 * deltaRot2 + a2 * deltaTrans * deltaTrans;
    double ddeltaRot2_ = deltaRot2 - rng.gaussian(sqrt(variance3));

    // compute the new coordinates and heading angle
    double angle = pc.theta + ddeltaRot1_;

    int xx = pc.x + ddeltaTrans_ * cos(angle);
    int yy = pc.y + ddeltaTrans_ * sin(angle);

    double ttheta = pc.theta + ddeltaRot1_ + ddeltaRot2_;
    if (ttheta < -M_PI)
      ttheta += 2 * M_PI;
    if (ttheta > M_PI)
      ttheta -= 2 * M_PI;

    // check validity of the new coordinates and truncate them if necessary
    if (!map.isFreeSpace(xx, yy)) {
      pc.w *= 1e-3;
      continue;
    }

    // update the state of a particle
    pc.x = xx;
    pc.y = yy;
    pc.theta = ttheta;
  }

  // normalize weights to prevent numerical issues
  renormalize();

  return robotMoved;
}

/* ************************************************************************* */
void ParticleFilter::rayCasting(const int x, const int y, const int theta,
                                vector<int> &zc) {
  double zMax = 840.0;
  static cv::Mat tempMat(map.size_y, map.size_x, CV_8UC1);

  cv::Point p0(x, y); // start point

#ifdef USEOMP
#pragma omp parallel for num_threads(params.nThreads)
#endif
  for (int idx = 0; idx < 180; idx++) {
    double angle = theta + (idx-90.0) / 180.0 * M_PI;
    // end point in one perticular direction
    cv::Point p2(x + zMax * cos(angle), y + zMax * sin(angle));

    cv::LineIterator it(tempMat, p0, p2, 8);
    int ptIdx = 0;
//    cv::Point lastPt;
    for (; ptIdx < it.count; ptIdx++, ++it) {
      if (ptIdx == it.count - 1) {
//        lastPt.x = it.pos().x;
//        lastPt.y = it.pos().y;
        zc[idx] = zMax;
      }

      // This is an approximation.
      double v = map.prob[it.pos().x][it.pos().y];
      if (v >= 0.5 || v < 0) {
        // we hit the wall now
        int dist = static_cast<int>(cv::norm(p0 - it.pos()) * 10.0);
        zc[idx] = dist; break;
      }
    }
  }
}

/* ************************************************************************* */
void ParticleFilter::updateMeasurementModel(const Reading& r) {
  static double inv_var_2 = 1e-5;   // it is the 1 / (2*\sigma^2).
  static double K = 2.5*1e3;    // robust kernel to truncate super-big error
  static double C = 1;    // a scale parameter for likelihood
  double x_off = (r.x_l - r.x_r)/10.0;
  double y_off = (r.y_l - r.y_r)/10.0;

  for (size_t pIdx = 0; pIdx < particles.size(); pIdx++) {
    vector<int> zc(180, 0.0);
    const Particle& pc = particles[pIdx];
    if (map.isFreeSpace(pc.x + x_off, pc.y + y_off)) {
      rayCasting(pc.x + x_off, pc.y + y_off, pc.theta, zc);
    } else {
      particles[pIdx].w = 0; continue;
    }

    double log_L = 0.0; // this is the log of likelihood function
    for (size_t lIdx = 0; lIdx < r.rs.size(); lIdx++) {
      // the number 25 is the system distance between the robot and measurement
      double error = min((static_cast<double>(zc[lIdx] - r.rs[lIdx])) *
          (static_cast<double>(zc[lIdx] - r.rs[lIdx])), K);
      log_L -= error;
    }
    double p = C*exp(log_L  * inv_var_2) + 1e-2; // Systematic error 1e-2
    particles[pIdx].w = particles[pIdx].w * p;

//    displayRays(pc.x + x_off, pc.y + y_off, pc.theta, zc);
  }

  renormalize();
}

/* ************************************************************************* */
void ParticleFilter::resampling() {
  size_t nrParticles = particles.size();

  // low variance resampling
  // I think the weight should be normalized in the summation below.
  vector<Particle> tempParticles;
  cv::RNG rng;
  double r = rng.uniform(0.0, 1.0 / static_cast<double>(nrParticles));
  double c = particles[0].w;
  size_t i = 0;
  for (size_t pIdx = 0; pIdx < nrParticles; pIdx++) {
    double U = r + static_cast<double>(pIdx) / static_cast<double>(nrParticles);
    while (U > c) {
      i++;
      c += (particles[i].w);
    }
    particles[i].w = 1.0 / nrParticles; // set the weight to 1
    // add particles around the particle variance
    const Particle& candidate = particles[i];
    tempParticles.push_back(candidate);
  }

  // get a new vector of particles
  particles.clear();
  particles.insert(particles.end(), tempParticles.begin(), tempParticles.end());
}

/* ************************************************************************* */
bool ParticleFilter::isResamplingNeeded(bool isOdometryChanged) {
  // If the odometry does not change, resampling is suspended.
  // This is an approximation to the case in which the state is static.
  // @Zhaoyang: this step is trivial. We can simply choose not to resample if
  // the measurement model is not updated.
  if (!isOdometryChanged) {
    return false;
  }

  // calculate the mean and standard deviation of the weights
  cv::Mat weightsMat(particles.size(), 1, CV_64FC1);
  for (size_t pIdx; pIdx < particles.size(); pIdx++) {
    weightsMat.at<double>(pIdx, 0) = particles[pIdx].w;
  }
  cv::Scalar mean;
  cv::Scalar stdDev;
  cv::meanStdDev(weightsMat, mean, stdDev);

  // If the std of weights is small or if the mean is small, we should do resampling
  double stdDevThresh = 0.3;
  if (stdDev.val[0] < stdDevThresh)
    return true;
  else
    return false;
}

/* ************************************************************************* */
double ParticleFilter::truncateAngle(double a) {
  if (a < -M_PI)
    return a + 2 * M_PI;
  if (a > M_PI)
    return a - 2 * M_PI;
  else
    return a;
}

/* ************************************************************************* */
void ParticleFilter::renormalize() {
  double sumWeights = 0.0;
  size_t nrParticles = particles.size();
  for (size_t pIdx = 0; pIdx < nrParticles; pIdx++) {
    sumWeights += particles[pIdx].w;
  }
  for (size_t pIdx = 0; pIdx < nrParticles; pIdx++) {
    particles[pIdx].w = particles[pIdx].w / sumWeights;
  }
}

