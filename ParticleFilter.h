/**
 * @file ParticleFilter.h
 * @date Feb. 2016
 * @author Zhaoyang Lv, Yao Chen
 */

#pragma once

#include "io.h"
#include "vis.h"
#include <opencv2/core/core.hpp>

#include <vector>
#include <string>

#ifdef USEOMP
#include <omp.h>
#endif

struct Particle {
  double x;   ///< x coordinate (column)
  double y;   ///< y coordinate (row)
  double theta;  ///< heading (radians)

  double w;   ///< weight of particles

  static double x_off; ///< the offset in x (column) of laser w.r.t. odometry
  static double y_off; ///< the offset in y (row) of laser w.r.t. odometry
  static double ts;  ///< timestamp of all particles
};

typedef std::vector<Particle> Particles;

class ParticleFilter {

  struct Parameters {
    uint nThreads;  // number of threads for this program
    bool visualize;
  } params;

public:
  /** Constructor
   * @param file of the map
   * @param file of robot reading
   * @param number of particles
   */
  ParticleFilter(const std::string& mapFile, const std::string& roboFile,
                 int size = 1e3) {

    read_beesoft_map(mapFile.c_str(), &map);
    read_log(roboFile, readings);

    initParticles(size);

    params.nThreads = 8;  // initialize the number of threads to run it.
#ifdef USEOMP
    params.nThreads = std::min(params.nThreads,uint(omp_get_max_threads()));
#endif

    params.visualize = true;
    visMap = visualizeMap(map);
  }

  ~ParticleFilter() {
  }

  /** Run the particle filter algorithm
   */
  void run();

  /** display all the particles
   * @param wait (ms) for display
   */
  void displayParticles(int wait = 1);

  /** Display the particle and its ray
   * @param x
   * @param y
   * @param zs laser reading 180 degree
   * @param wait (ms) for display
   */
  void displayRays(int x, int y, int theta,
                   const std::vector<int>& zs, int wait = 0);

private:

  /** Initialize particles, randomly distributed in the map
   * @param size
   */
  void initParticles(int size);

  /** Update the particle motion model
   *  The motion model follows the algorithm in Table 5.6 of the textbook
   * @param current odometry reading
   * @param last odometry reading
   * @return whether odometry changes
   */
  bool updateMotionModel(const Reading& currOdometry,
                         const Reading& lastOdometry);

  /** Update the weights of particles
   * @param current odometry reading
   */
  void updateMeasurementModel(const Reading& r);

  /** resampling particles
   *
   *  algorithm in Table 4.4
   */
  void resampling();

  /** check whether we need to resample the particles
   */
  bool isResamplingNeeded(bool isOdometryChanged);

  /** ray casting
   * @param x
   * @param y
   * @param theta
   */
  void rayCasting(const int x, const int y, const int theta,
                  std::vector<int> &zc);

  /** Renormalize the weights of all the particles
   */
  void renormalize();

  /** truncate angle
   */
  double truncateAngle(double a);

  Particles particles;      ///< all the particles

  Map map;     ///< the map of the environment

  Readings readings;      ///< laser readings

  cv::Mat visMap;   ///< a cached of visualized map
};



