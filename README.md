# Particle Filter Based Location #
We implemented a localization algorithm based on the particle filtering. With OpenMP option on, we are able to localize 1000 particles at the frame rate of 4Hz. For more details, please refer to the documentation folder.

### How do I get set up? ###

```
#!Linux
cd /path/to/pflocalization
mkdir build
cd build
cmake ..
make -j4
```

### How do I run the executables? ###
Suppose you are at `/path/to/pflocalization/build`. You can try:
```
#!Linux
./main
```