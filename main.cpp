#include "io.h"
#include "vis.h"
#include "ParticleFilter.h"
#include <opencv2/highgui/highgui.hpp>

#include <string>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {

  int dataNum = 1;

  if (argc > 1) {
    dataNum = atoi(argv[1]);
    cout << "choose data log: " << dataNum << endl;
  }

  string file = "../data/map/wean.dat";
  string laserFile;

  switch(dataNum) {
    case 1: {
      laserFile = "../data/log/robotdata1.log"; break;
    }
    case 2: {
      laserFile = "../data/log/ascii-robotdata2.log"; break;
    }
    case 3: {
      laserFile = "../data/log/ascii-robotdata3.log"; break;
    }
    case 4: {
      laserFile = "../data/log/ascii-robotdata4.log"; break;
    }
    case 5: {
      laserFile = "../data/log/ascii-robotdata5.log"; break;
    }
  }

  ParticleFilter filter4data(file, laserFile, 1e3);
  filter4data.run();

}
