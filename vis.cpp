/**
 * @file vis.cpp
 * @brief this is for the visualization utilities
 * @author Zhaoyang Lv
 */

#include "vis.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


cv::Mat visualizeMap(const Map& map) {

  cv::Mat disp(map.size_y, map.size_x, CV_8UC3, cvScalar(0, 0, 0));

  for (int x = 0; x < map.size_x; x++) {
    for (int y = 0; y < map.size_y; y++) {
      double m = map.prob[x][y];

      if (m < 0) {
        disp.at<cv::Vec3b>(map.size_y - y - 1, x) = cv::Vec3b(50, 0, 0);
        continue;
      }

      cv::Vec3b color = m * cv::Vec3b(255, 255, 255);
      disp.at<cv::Vec3b>(map.size_y - y - 1, x) =  color;
    }
  }

  return disp;
}

void visualizeReadings(const Readings& readings, const Map& map) {

  double x_0(0), y_0(0), theta_0(0);
  for (int i = 0; i < readings.size(); i++) {
    cv::Mat disp(map.size_x, map.size_y, CV_8UC3, cvScalar(0,0,0));

    const Reading& r = readings[i];
    if (i < 1) {
      x_0 = r.x_r;
      y_0 = r.y_r;
      theta_0 = r.theta_r;
    }

    double x_t1 = double(r.x_r - x_0)/map.resolution + map.size_x / 2;
    double y_t1 = double(r.y_r - y_0)/map.resolution + map.size_y / 2;
    double theta_t1 = r.theta_r - theta_0;

    // draw the robot
    // suppose the size of the robot is 200 * 200;
    // @todo: it does not plot the robot heading
    cv::Rect robot(x_t1 - 10, y_t1 - 10, 20, 20);
    cv::Point2i roboC(y_t1, x_t1);
    cv::Point2i laserC(y_t1, x_t1);
    cv::rectangle(disp, robot, cv::Scalar(255, 0, 0), 4, -1);
    cv::arrowedLine(disp, laserC,
                    cv::Point2i(laserC.y + cos(theta_t1)*10,
                                laserC.x + sin(theta_t1)*10),
                    cv::Scalar(255, 0, 0), 3);
    if (!r.isLaser()) continue;

    for (int j = 0; j < r.rs.size(); j++) {
      int len = double(r.rs[j])/map.resolution;
      double theta = theta_t1 -j / 180.0 * M_PI - M_PI_2;
      cv::Point2i endP(laserC.y + cos(theta)*len, laserC.x + sin(theta)*len);
      cv::line(disp, laserC, endP, cv::Scalar(255,255,255), 2);
    }

    cv::imshow("laser reading", disp);
    cv::waitKey(5);
  }

}

