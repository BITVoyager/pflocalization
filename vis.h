/**
 * @file vis.h
 * @brief this is for the visualization utilities
 * @author Zhaoyang Lv
 */

#pragma once

#include "io.h"
#include <opencv2/core/core.hpp>

cv::Mat visualizeMap(const Map& map);

void visualizeReadings(const Readings& readings, const Map& map);
